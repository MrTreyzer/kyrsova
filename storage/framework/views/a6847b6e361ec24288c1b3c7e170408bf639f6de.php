<nav>
    <a id="red" class="menu" href="/home">home</a>
    <a class="menu" href="/trip">Розклад рейсів</a>
    <a class="menu" href="/findTrip">Знайти рейс</a>
    <?php if(auth()->guard()->check()): ?>
    <a class="menu" href="/addTrip">Додоти рейс</a>
    <a class="menu" href="/delTrip">Видалити рейс</a>
    <a class="menu" href="/updateTrip">Редагувати рейс</a>
    <?php endif; ?>
</nav>
<?php /**PATH C:\OpenServer\domains\kyrsova\resources\views/app/static/menu.blade.php ENDPATH**/ ?>