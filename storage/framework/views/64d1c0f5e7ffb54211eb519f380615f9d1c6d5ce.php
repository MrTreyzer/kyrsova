<?php $__env->startSection("first"); ?>
    <?php echo $__env->make('app.nstatic.firstText', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <form id="bus" align="center" action="/trip">
        <nav>Рейси атобусів відсортованих</nav><br>
        <select class="form-group-last" name="sort_id">
            <option value="1" <?php echo e(( 1 == $sort_id ) ? 'selected' : ''); ?>>Місце призначення</option>
            <option value="2" <?php echo e(( 2 == $sort_id ) ? 'selected' : ''); ?>>Номер рейсу</option>
            <option value="3" <?php echo e(( 3 == $sort_id ) ? 'selected' : ''); ?>>Дата прибуття</option>
            <option value="4" <?php echo e(( 4 == $sort_id ) ? 'selected' : ''); ?>>Перевізник</option>
            <option value="5" <?php echo e(( 5 == $sort_id ) ? 'selected' : ''); ?>>Продано білетів</option>
        </select>

        <input class="menu2" type="submit" value="Показати"/>
    </form>
    <?php echo $__env->make('app.nstatic.printTrip', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app.static.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OpenServer\domains\kyrsova\resources\views/app/nstatic/trip.blade.php ENDPATH**/ ?>