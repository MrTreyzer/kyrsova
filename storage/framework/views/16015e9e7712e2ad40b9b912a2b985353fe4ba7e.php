<?php $__env->startSection("first"); ?>
    <?php echo $__env->make('app.nstatic.firstText', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <form id="bus" align="center">
        <nav>Знайти рейс автобусів</nav><br>
        <select class="form-group" name="typeFind">
            <option value="1" <?php echo e(( 1 == $typeFind ) ? 'selected' : ''); ?>>Місце призначення</option>
            <option value="2" <?php echo e(( 2 == $typeFind ) ? 'selected' : ''); ?>>Номер рейсу</option>
            <option value="3" <?php echo e(( 3 == $typeFind ) ? 'selected' : ''); ?>>Дата прибуття</option>
            <option value="4" <?php echo e(( 4 == $typeFind ) ? 'selected' : ''); ?>>Перевізник</option>
        </select>
        <input placeholder="Введіть дані для пошуку" class="form-group-last" required type="text" name="dataFind" id="subject" value="<?php if(isset($_GET['dataFind'])){ print $_GET['dataFind']; } ?>">
        <input class="menu2" type="submit" value="Показати"/>
    </form>
    <?php echo $__env->make('app.nstatic.printTrip', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app.static.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OpenServer\domains\kyrsova\resources\views/app/nstatic/findTrip.blade.php ENDPATH**/ ?>