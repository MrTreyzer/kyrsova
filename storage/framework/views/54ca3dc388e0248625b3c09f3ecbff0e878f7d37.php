<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo e(asset("css.css")); ?>">
    <link rel="shortcut icon" href="<?php echo e(asset("favicon.png")); ?>" type="image/x-icon">
    <link rel="" href="" type="">
    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
    <title>Доставка Людей</title>
</head>
<body>
<div  id="first">
    <div class="telo">
        <?php echo $__env->make('app.static.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->yieldContent('first'); ?>
    </div>
</div>

<div class="telo">
    <?php echo $__env->yieldContent('page_title'); ?>
    <br/><br/>
    <div class="container">
        <?php echo $__env->yieldContent('content'); ?>
    </div>
    <br/>
    <br/>

</div>
<?php echo $__env->make('app.static.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\OpenServer\domains\kyrsova\resources\views/app/static/layout.blade.php ENDPATH**/ ?>