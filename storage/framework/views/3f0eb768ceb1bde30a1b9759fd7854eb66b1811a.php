<?php $__env->startSection("first"); ?>
    <?php echo $__env->make('app.nstatic.firstText', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_title'); ?>
    <nav>Рейс № <?php echo e($trips->flight_number); ?></nav>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <table class="table_col" align="center" width="100%">
        <colgroup>
            <col style="background:#ff2640;">
        </colgroup>
        <th>Місце призначення</th>
        <th>Дата прибуття</th>
        <th>Перевізник</th>
        <th>Доступно білетів</th>
            <tr>
                <td><?php echo e($trips->destination); ?></td>
                <td><?php echo e($trips->date_of_departure); ?></td>
                <td><?php echo e($trips->carrier); ?></td>
                <td><?php echo e($trips->tickets); ?></td>
            </tr>
    </table>
    <br>
    <a class="menu2" href="/trip">Дивитися всі рейси</a>
    <?php if(auth()->guard()->check()): ?>
        <form id="bus" align="center">
        <input name="buy" class="menu2" type="submit" value="Купити"/>
    </form>
    <?php echo e($addusersTrip); ?>

    <?php endif; ?>
    <?php if(auth()->guard()->guest()): ?>
        <p>Увійдіть в акаутн щоб купити білет</p>
    <?php endif; ?>
    <style>
        .table_col {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 14px;
            width: 100%;
            background: white;
            text-align: left;
            border-collapse: collapse;
            color: #3E4347;
        }
        .table_col th:first-child, .table_col td:first-child {
            color: #F5F6F6;
            border-left: none;
        }
        .table_col th {
            font-weight: normal;
            border-bottom: 2px solid rgba(0, 0, 0, 0.4);
            border-right: 20px solid white;
            border-left: 20px solid white;
            padding: 8px 10px;
        }
        .table_col td {
            border-right: 20px solid white;
            border-left: 20px solid white;
            padding: 12px 10px;
            color: #8b8e91;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app.static.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OpenServer\domains\kyrsova\resources\views/app/nstatic/oneTrip.blade.php ENDPATH**/ ?>