<?php $__env->startSection("first"); ?>
    <?php echo $__env->make('app.nstatic.firstText', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <nav>Редагувати рейс автобусів</nav><br>
    <?php if(auth()->guard()->check()): ?>
    <form id="bus" align="center">

        <input placeholder="Номер рейсу" class="form-group" required type="number" name="flight_number" id="subject3" value="<?php if(isset($_GET['flight_number'])){ print $_GET['flight_number']; } ?>"><br>
        <nav> Нові данні  :</nav><br>
    <!--<span> Місце призначення  :</span><br>-->
        <select class="form-group" name="destination">
            <option value="">Місце призначення</option>
            <?php $__currentLoopData = $getDestination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($trip->destination); ?>"<?php echo e(( $trip->destination == $destination ) ? 'selected' : ''); ?>><?php echo e($trip->destination); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select><br>
        <input class="form-group" type="date" name="date_of_departure" id="subject1" value="<?php if(isset($_GET['date_of_departure'])){ print $_GET['date_of_departure']; } ?>"><br>
        <!--<span> Перевізник  :</span><br>-->
        <select class="form-group" name="carrier">
            <option value="">Перевізник</option>
            <?php $__currentLoopData = $getCarrier; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($trip->carrier); ?>"<?php echo e(( $trip->carrier == $carrier ) ? 'selected' : ''); ?>><?php echo e($trip->carrier); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select><br>
        <input placeholder="Кількість білетів" class="form-group-last" type="number" name="tickets" id="subject2" value="<?php if(isset($_GET['tickets'])){ print $_GET['tickets']; } ?>">
        <input class="menu2" type="submit" value="Змінити"/>
    </form>
    <?php echo $__env->make('app.nstatic.printTrip', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
    <?php if(auth()->guard()->guest()): ?>
        <H1  align="center">Увійдіть в акаутн щоб редагувати рейси</H1>
        <br>
        <br>
        <br>
        <br>
        <br>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app.static.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OpenServer\domains\kyrsova\resources\views/app/nstatic/updateTrip.blade.php ENDPATH**/ ?>