<head>
    <style>
        .table_col {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 14px;
            width: 100%;
            background: white;
            text-align: left;
            border-collapse: collapse;
            color: #3E4347;
        }
        .table_col th:first-child, .table_col td:first-child {
            color: #F5F6F6;
            border-left: none;
        }
        .table_col th {
            font-weight: normal;
            border-bottom: 2px solid rgba(0, 0, 0, 0.4);
            border-right: 20px solid white;
            border-left: 20px solid white;
            padding: 8px 10px;
        }
        .table_col td {
            border-right: 20px solid white;
            border-left: 20px solid white;
            padding: 12px 10px;
            color: #8b8e91;
        }
    </style>
</head>
<table class="table_col" align="center" width="100%">
    <colgroup>
        <col style="background:#ff2640;">
    </colgroup>
    <th>Місце призначення</th>
    <th>Номер рейсу</th>
    <th>Дата прибуття</th>
    <th>Перевізник</th>
    <th>Доступно білетів</th>
    <?php $__currentLoopData = $trips; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($trip->destination); ?></td>
            <td><?php echo e($trip->flight_number); ?></td>
            <td><?php echo e($trip->date_of_departure); ?></td>
            <td><?php echo e($trip->carrier); ?></td>
            <td><?php echo e($trip->tickets); ?></td>
            <td><a class="menu2" href="/trip/<?php echo e($trip->flight_number); ?>">Детально</a></td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>
<?php /**PATH C:\OpenServer\domains\kyrsova\resources\views/app/nstatic/printTrip.blade.php ENDPATH**/ ?>