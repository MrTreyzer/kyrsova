<?php $__env->startSection("first"); ?>
    <?php echo $__env->make('app.nstatic.firstText', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <nav> Додати рейс автобусів</nav><br>
    <?php if(auth()->guard()->check()): ?>
    <form id="bus" align="center">

        <select class="form-group" name="destination">
            <option required value="">Місце призначення</option>
            <?php $__currentLoopData = $getDestination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($trip->destination); ?>"<?php echo e(( $trip->destination == $destination ) ? 'selected' : ''); ?>><?php echo e($trip->destination); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select><br>
        <input required class="form-group" required type="date" name="date_of_departure" id="subject1" value="<?php if(isset($_GET['date_of_departure'])){ print $_GET['date_of_departure']; } ?>"><br>
        <select required class="form-group-last" name="carrier">
            <option value="">Перевізник</option>
            <?php $__currentLoopData = $getCarrier; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($trip->carrier); ?>"<?php echo e(( $trip->carrier == $carrier ) ? 'selected' : ''); ?>><?php echo e($trip->carrier); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>

        <input class="menu2" type="submit" value="Додати"/>
    </form>
    <?php echo $__env->make('app.nstatic.printTrip', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
    <?php if(auth()->guard()->guest()): ?>
        <H1  align="center">Увійдіть в акаутн щоб додати рейси</H1>
        <br>
        <br>
        <br>
        <br>
        <br>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app.static.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OpenServer\domains\kyrsova\resources\views/app/nstatic/addTrip.blade.php ENDPATH**/ ?>