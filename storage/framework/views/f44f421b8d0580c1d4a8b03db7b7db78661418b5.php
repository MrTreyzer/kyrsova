<?php $__env->startSection("first"); ?>
    <?php echo $__env->make('app.nstatic.firstText', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <nav>Видалити рейс автобусів</nav><br>
    <?php if(auth()->guard()->check()): ?>
    <form id="bus" align="center">

        <input placeholder="Номер рейсу" class="form-group-last" required type="number" name="flight_number" min="1" id="subject" value="<?php if(isset($_GET['flight_number'])){ print $_GET['flight_number']; } ?>">
        <input class="menu2" type="submit" value="Видалити"/>
    </form>
    <?php echo $__env->make('app.nstatic.printTrip', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
    <?php if(auth()->guard()->guest()): ?>
        <H1  align="center">Увійдіть в акаутн щоб видалити рейси</H1>
        <br>
        <br>
        <br>
        <br>
        <br>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app.static.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OpenServer\domains\kyrsova\resources\views/app/nstatic/delTrip.blade.php ENDPATH**/ ?>