<?php

namespace App\Http\Controllers;

use App\Models\rais;
use App\Models\TripEdit;
use Illuminate\Http\Request;

class raisController extends Controller
{

    public function trip(Request $request){
        $sort = $request->input('sort_id', null);
        $model_trips = new rais();
        $trips = $model_trips->getTrips($sort);
        $usersTrip = $model_trips->printTripUser();
        $usersTripT= $model_trips->pr($usersTrip);
        return view('app.nstatic.trip', [
            'trips' => $trips,
            'usersTrip'=>$usersTrip,
                'usersTripT'=>$usersTripT,
            'sort_id'=>$sort]
        );
    }
    public function oneTrip($id,Request $request){
        $buy = $request->input('buy', null);
        $model_trip = new rais();
        $model_trip2 = new TripEdit();
        $addusersTrip = $model_trip2->addTripUser($id,$buy);
        $trips = $model_trip->getTripBy($id);
        $usersTrip = $model_trip->printTripUser();
        $usersTripT= $model_trip->pr($usersTrip);
        return view('app.nstatic.oneTrip',
            ['usersTrip'=>$usersTrip,
            'addusersTrip'=>$addusersTrip,
            'usersTripT'=>$usersTripT,])->with('trips', $trips);
    }
    public  function findTrip(Request $request){
        $typeFind= $request->input('typeFind', null);
        $dataFind= $request->input('dataFind', null);
        $model_trips = new rais();
        $trips = $model_trips->getTripB($typeFind,$dataFind);
        $usersTrip = $model_trips->printTripUser();
        $usersTripT= $model_trips->pr($usersTrip);
        return view('app.nstatic.findTrip' ,[
            'trips' => $trips,
            'usersTrip'=>$usersTrip,
            'usersTripT'=>$usersTripT,
            'typeFind' => $typeFind,
            'dataFind'=>$dataFind]);
    }

}
