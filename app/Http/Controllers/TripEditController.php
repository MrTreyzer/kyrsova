<?php

namespace App\Http\Controllers;

use App\Models\rais;
use App\Models\TripEdit;
use Illuminate\Http\Request;

class TripEditController extends Controller
{
    public function addTrip(Request $request){
        $destination= $request->input('destination', null);
        $date_of_departure= $request->input('date_of_departure', null);
        $carrier= $request->input('carrier', null);
        $model_trips = new rais();
        $model_trip2 = new TripEdit();
        $trips = $model_trip2->addTrip($destination,$date_of_departure,$carrier);
        $getCarrier = $model_trips->getCarrier();
        $getDestination = $model_trips->getDestination();
        $usersTrip = $model_trips->printTripUser();
        $usersTripT= $model_trips->pr($usersTrip);
        return view('app.nstatic.addTrip',[
            'trips' => $trips,
            'usersTrip'=>$usersTrip,
            'usersTripT'=>$usersTripT,
            'getCarrier' => $getCarrier,
            'destination' => $destination,
            'getDestination' => $getDestination,
            'date_of_departure' => $date_of_departure,
            'carrier'=>$carrier]);
    }
    public function updateTrip(Request $request){
        $flight_number= $request->input('flight_number', null);
        $destination= $request->input('destination', null);
        $date_of_departure= $request->input('date_of_departure', null);
        $carrier= $request->input('carrier', null);
        $tickets= $request->input('tickets', null);
        $model_trips = new rais();
        $model_trip2 = new TripEdit();
        $trips = $model_trip2->updateTrip($flight_number,$destination,$date_of_departure,$carrier,$tickets);
        $getCarrier = $model_trips->getCarrier();
        $getDestination = $model_trips->getDestination();
        $usersTrip = $model_trips->printTripUser();
        $usersTripT= $model_trips->pr($usersTrip);
        return view('app.nstatic.updateTrip',[
            'trips' => $trips,
            'usersTrip'=>$usersTrip,
            'usersTripT'=>$usersTripT,
            'getCarrier' => $getCarrier,
            'getDestination' => $getDestination,
            'flight_number' => $flight_number,
            'tickets' => $tickets,
            'destination' => $destination,
            'date_of_departure' => $date_of_departure,
            'carrier'=>$carrier]);
    }
    public function delTrip(Request $request){
        $flight_number= $request->input('flight_number', null);
        $model_trips = new rais();
        $model_trip2 = new TripEdit();
        $trips = $model_trip2->delTrip($flight_number);
        $usersTrip = $model_trips->printTripUser();
        $usersTripT= $model_trips->pr($usersTrip);
        return view('app.nstatic.delTrip', [
                'trips' => $trips,
                'usersTrip'=>$usersTrip,
                'usersTripT'=>$usersTripT,
                'flight_number'=>$flight_number]
        );

    }
}
