<?php

namespace App\Http\Controllers;

use App\Models\rais;
use App\Models\TripEdit;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request){
        $buy= $request->input('nebuy', null);
        $model_trips = new rais();
        $model_trip = new TripEdit();
        $removeUsersTrip = $model_trip->removeTripUser($buy);
        $usersTrip = $model_trips->printTripUserFull();
        return view('home',['usersTrip'=>$usersTrip]);
    }
}
