<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TripEdit extends Model
{
    public function addTrip($destination,$date_of_departure,$carrier){
        $trip = DB::table('table');
        if ($destination and $date_of_departure and $carrier ==!null){$trip->insert(['destination' => $destination,
            'date_of_departure'=>$date_of_departure,
            'carrier'=>$carrier]);
            $trip->latest('flight_number')->first();
        }
        else {$trip->select('*');
            $trip->orderBy('destination');}
        $addtrip=$trip->get();
        return $addtrip;
    }
    public function updateTrip($id,$destination,$date_of_departure,$carrier,$tickets){
        $trip = DB::table('table');
        $trip->where('flight_number',$id);
        if($destination==!null)$trip->update(['destination'=>$destination]);
        if($date_of_departure==!null)$trip->update(['date_of_departure'=>$date_of_departure]);
        if($carrier==!null)$trip->update(['carrier'=>$carrier]);
        if($tickets==!null)$trip->update(['tickets'=>$tickets]);
        $updateTrip=$trip->get();
        return $updateTrip;
    }
    public  function delTrip($id){

        if ($id==!null){
            $trip = DB::table('table');
            $trip->where('flight_number',$id);
            $trip->delete();
            $trip->get();
        }
        $trips = DB::table('table');
        $trips->select('*');
        $trips->orderBy('flight_number');
        $addtrip=$trips->get();
        return $addtrip;
    }
    public  function addTripUser($flight_number,$buy){
        $do='';
        if ($buy==!null){
            $do='невдалось купить білет';
            if (Auth::check()) {
                $user = Auth::user()->id;
                $query = DB::table('usertrip');

                $re = DB::table('table')->select('*')
                    ->where('flight_number', $flight_number)
                    ->select('tickets')
                    ->get();
                $req=$re->toArray();
                if($req[0]->tickets==!0){ DB::table('table')->select('*')
                    ->where('flight_number', $flight_number)->decrement('tickets');
                    $query->insert(['userid' => $user,
                        'TripId'=>$flight_number]);
                    $do="успішно";
                }
                else $do=' білети закінчились ';
                $query->get();

            }
        }
        return $do;
    }
    public function removeTripUser($buy){
        if ($buy==!null){


            if (Auth::check()) {
                $user = Auth::user()->id;
                $query = DB::table('usertrip');
                $query->where('userid',$user);
                $query->where('TripId',$buy)->first();
                $query->delete();
                DB::table('table')->select('*')
                    ->where('flight_number', $buy)->increment('tickets');
            }
        }
    }
}
