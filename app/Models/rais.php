<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class rais extends Model
{
    public function getTrips($sort)
    {
        $query = DB::table('table');
        $query->select(
            'destination','flight_number','date_of_departure','tickets','carrier');
        if($sort==1)$query->orderBy('destination');
        elseif ($sort==2)$query->orderBy('flight_number');
        elseif ($sort==3)$query->orderBy('date_of_departure');
        elseif ($sort==5)$query->orderBy('tickets');
        elseif ($sort==4)$query->orderBy('carrier');
        $trips = $query->get();
        return $trips;
    }
    public function getTripBy($id){
        if(!$id) return null;
        $trip = DB::table('table')
            ->select('*')
            ->where('flight_number', $id)
            ->get()->first();
        return $trip;
    }
    public function getTripB($id,$data){
        $trip = DB::table('table');
        $trip->select('*');
        if($id==2) $trip->where('flight_number', $data) ;
        elseif ($id==1)$trip->where('destination', $data) ;
        elseif ($id==3)$trip->where('date_of_departure', $data) ;
        elseif ($id==4)$trip->where('carrier', $data) ;
        $ftrip=$trip->get();
        return $ftrip;
    }

    public function getCarrier(){
        $trip = DB::table('table');
        $trip->select('carrier');;
        $addtrip=$trip->distinct();
        return $addtrip->get();
    }
    public function getDestination(){
        $trip = DB::table('table');
        $trip->select('destination');;
        $addtrip=$trip->distinct();
        return $addtrip->get();
    }

    public  function printTripUser(){
        $query1=null;
        if (Auth::check()) {
            $user = Auth::user()->id;
            $query = DB::table('table')->join('usertrip', 'table.flight_number', '=', 'usertrip.TripId');
            $query->where('userid',$user);
            $query->select(
                'destination','flight_number','date_of_departure','tickets','carrier');
            $query->orderBy('date_of_departure')->take(5);
            $query1=$query->get();
        }
        return $query1;
    }
    public  function printTripUserFull(){
        $query1=null;
        if (Auth::check()) {
            $user = Auth::user()->id;
            $query = DB::table('table')->join('usertrip', 'table.flight_number', '=', 'usertrip.TripId');
            $query->where('userid',$user);
            $query->select(
                'destination','flight_number','date_of_departure','tickets','carrier');
            $query->orderBy('date_of_departure');
            $query1=$query->get();
        }
        return $query1;
    }
    public function pr($usersTrip){
        $usersTripT=false;
        if (Auth::check()) {
        $usersTripT = $usersTrip->toArray();
        if (empty($usersTripT[0])) $usersTripT==true;
        else $usersTripT==false;}
        return $usersTripT;
    }
}
