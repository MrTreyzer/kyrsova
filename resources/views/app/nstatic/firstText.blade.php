
        <div id="glavtext">
            <div id="vid">
                <img src="{{ asset("bus2.gif") }}" width="300" height="225" />
            </div>
            @auth()
                @if($usersTripT)
                <p>Ваші рейси:</p>
                <table>
                    <th>Місце призначення</th>
                    <th>Номер рейсу</th>
                    <th>Дата прибуття</th>
                    <th>Перевізник</th>
                    @foreach ($usersTrip as $trip)
                        <tr>
                            <td>{{ $trip->destination}}</td>
                            <td>{{ $trip->flight_number }}</td>
                            <td>{{ $trip->date_of_departure }}</td>
                            <td>{{ $trip->carrier }}</td>
                        </tr>
                    @endforeach
                </table>
                @else <h1 class="tex">Доставка людей<br> майбутньго </h1>
                <span class="tex2">Ми ДОСТАВКА ЛЮДЕЙ МАЙБУТНЬГО кращий сервіс для<br>
                вбору рейсу автобусів.<br>
                У вас зараз немає ніодного білете.Після придбання вони відобразятся тут</span>
                @endif

            @endauth
            @guest
                <h1 class="tex">Доставка людей<br> майбутньго </h1>
                <span class="tex2">Ми ДОСТАВКА ЛЮДЕЙ МАЙБУТНЬГО кращий сервіс для<br>
                вбору рейсу автобусів.<br>
                Для того щоб керувати рейсами зайдіть на свій акаунт або створіть новий</span>
            @endguest
            <div id="posit">
                @if (Route::has('login'))
                    @auth
                        <a style="text-decoration: none" id="knopka1" href="{{ url('/home') }}">Переглянути повний список</a>
                        <a style="text-decoration: none" id="knopka1" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    @else
                        <a style="text-decoration: none" id="knopka1" href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a style="text-decoration: none" id="knopka1" href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
            </div>
            @endif
        </div>
