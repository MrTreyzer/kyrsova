@extends('app.static.layout')
@section("first")
    @include('app.nstatic.firstText')
@endsection
@section('content')
    <nav>Видалити рейс автобусів</nav><br>
    @auth()
    <form id="bus" align="center">

        <input placeholder="Номер рейсу" class="form-group-last" required type="number" name="flight_number" min="1" id="subject" value="<?php if(isset($_GET['flight_number'])){ print $_GET['flight_number']; } ?>">
        <input class="menu2" type="submit" value="Видалити"/>
    </form>
    @include('app.nstatic.printTrip')
    @endauth
    @guest()
        <H1  align="center">Увійдіть в акаутн щоб видалити рейси</H1>
        <br>
        <br>
        <br>
        <br>
        <br>
    @endguest
@endsection
