@extends('app.static.layout')
@section("first")
    @include('app.nstatic.firstText')
@endsection
@section('content')
    <form id="bus" align="center">
        <nav>Знайти рейс автобусів</nav><br>
        <select class="form-group" name="typeFind">
            <option value="1" {{ ( 1 == $typeFind ) ? 'selected' : '' }}>Місце призначення</option>
            <option value="2" {{ ( 2 == $typeFind ) ? 'selected' : '' }}>Номер рейсу</option>
            <option value="3" {{ ( 3 == $typeFind ) ? 'selected' : '' }}>Дата прибуття</option>
            <option value="4" {{ ( 4 == $typeFind ) ? 'selected' : '' }}>Перевізник</option>
        </select>
        <input placeholder="Введіть дані для пошуку" class="form-group-last" required type="text" name="dataFind" id="subject" value="<?php if(isset($_GET['dataFind'])){ print $_GET['dataFind']; } ?>">
        <input class="menu2" type="submit" value="Показати"/>
    </form>
    @include('app.nstatic.printTrip')
@endsection
