@extends('app.static.layout')
@section("first")
    @include('app.nstatic.firstText')
@endsection
@section('content')
    <nav>Редагувати рейс автобусів</nav><br>
    @auth()
    <form id="bus" align="center">

        <input placeholder="Номер рейсу" class="form-group" required type="number" name="flight_number" id="subject3" value="<?php if(isset($_GET['flight_number'])){ print $_GET['flight_number']; } ?>"><br>
        <nav> Нові данні  :</nav><br>
    <!--<span> Місце призначення  :</span><br>-->
        <select class="form-group" name="destination">
            <option value="">Місце призначення</option>
            @foreach($getDestination as $trip)
                <option value="{{ $trip->destination}}"{{ ( $trip->destination == $destination ) ? 'selected' : '' }}>{{ $trip->destination }}</option>
            @endforeach
        </select><br>
        <input class="form-group" type="date" name="date_of_departure" id="subject1" value="<?php if(isset($_GET['date_of_departure'])){ print $_GET['date_of_departure']; } ?>"><br>
        <!--<span> Перевізник  :</span><br>-->
        <select class="form-group" name="carrier">
            <option value="">Перевізник</option>
            @foreach($getCarrier as $trip)
                <option value="{{ $trip->carrier}}"{{ ( $trip->carrier == $carrier ) ? 'selected' : '' }}>{{ $trip->carrier }}</option>
            @endforeach
        </select><br>
        <input placeholder="Кількість білетів" class="form-group-last" type="number" name="tickets" id="subject2" value="<?php if(isset($_GET['tickets'])){ print $_GET['tickets']; } ?>">
        <input class="menu2" type="submit" value="Змінити"/>
    </form>
    @include('app.nstatic.printTrip')
    @endauth
    @guest()
        <H1  align="center">Увійдіть в акаутн щоб редагувати рейси</H1>
        <br>
        <br>
        <br>
        <br>
        <br>
    @endguest
@endsection
