<head>
    <style>
        .table_col {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 14px;
            width: 100%;
            background: white;
            text-align: left;
            border-collapse: collapse;
            color: #3E4347;
        }
        .table_col th:first-child, .table_col td:first-child {
            color: #F5F6F6;
            border-left: none;
        }
        .table_col th {
            font-weight: normal;
            border-bottom: 2px solid rgba(0, 0, 0, 0.4);
            border-right: 20px solid white;
            border-left: 20px solid white;
            padding: 8px 10px;
        }
        .table_col td {
            border-right: 20px solid white;
            border-left: 20px solid white;
            padding: 12px 10px;
            color: #8b8e91;
        }
        .menu3 {
            padding: 9px 20px 11px 20px;
            transition: 0.3s;
            margin: 5px;
            font-family: "Comic Sans MS";
            user-select: none;
            outline: none;
            text-decoration: none;
            color: #4c4c4c;
            transition: 0.5s;
            border: 1px;
            border-style: solid;
            border-color: white;
            border-radius: 4px;
        }
        .menu3:hover {
            border-color: red;
            border-radius: 3px;
        }
    </style>
</head>
<nav>Ваші рейси:</nav>
<table class="table_col" align="center" width="100%">
    <colgroup>
        <col style="background:#ff2640;">
    </colgroup>
    <th>Місце призначення</th>
    <th>Номер рейсу</th>
    <th>Дата прибуття</th>
    <th>Перевізник</th>
    @foreach ($usersTrip as $trip)
        <tr>
            <td>{{ $trip->destination}}</td>
            <td>{{ $trip->flight_number }}</td>
            <td>{{ $trip->date_of_departure }}</td>
            <td>{{ $trip->carrier }}</td>
            <td><form >
                    <input style="display: none;" name="nebuy" value={{$trip->flight_number}}>
                    <input  class="menu3" type="submit" value="відиовитися"/>
                </form></td>
        </tr>
    @endforeach
</table>
<br>
<br>
<br>
<br>

