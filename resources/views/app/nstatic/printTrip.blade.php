<head>
    <style>
        .table_col {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 14px;
            width: 100%;
            background: white;
            text-align: left;
            border-collapse: collapse;
            color: #3E4347;
        }
        .table_col th:first-child, .table_col td:first-child {
            color: #F5F6F6;
            border-left: none;
        }
        .table_col th {
            font-weight: normal;
            border-bottom: 2px solid rgba(0, 0, 0, 0.4);
            border-right: 20px solid white;
            border-left: 20px solid white;
            padding: 8px 10px;
        }
        .table_col td {
            border-right: 20px solid white;
            border-left: 20px solid white;
            padding: 12px 10px;
            color: #8b8e91;
        }
    </style>
</head>
<table class="table_col" align="center" width="100%">
    <colgroup>
        <col style="background:#ff2640;">
    </colgroup>
    <th>Місце призначення</th>
    <th>Номер рейсу</th>
    <th>Дата прибуття</th>
    <th>Перевізник</th>
    <th>Доступно білетів</th>
    @foreach ($trips as $trip)
        <tr>
            <td>{{ $trip->destination}}</td>
            <td>{{ $trip->flight_number }}</td>
            <td>{{ $trip->date_of_departure }}</td>
            <td>{{ $trip->carrier }}</td>
            <td>{{ $trip->tickets }}</td>
            <td><a class="menu2" href="/trip/{{ $trip->flight_number }}">Детально</a></td>
        </tr>
    @endforeach
</table>
