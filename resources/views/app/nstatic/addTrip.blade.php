@extends('app.static.layout')
@section("first")
    @include('app.nstatic.firstText')
@endsection
@section('content')
    <nav> Додати рейс автобусів</nav><br>
    @auth()
    <form id="bus" align="center">

        <select class="form-group" name="destination">
            <option required value="">Місце призначення</option>
            @foreach($getDestination as $trip)
                <option value="{{ $trip->destination}}"{{ ( $trip->destination == $destination ) ? 'selected' : '' }}>{{ $trip->destination }}</option>
            @endforeach
        </select><br>
        <input required class="form-group" required type="date" name="date_of_departure" id="subject1" value="<?php if(isset($_GET['date_of_departure'])){ print $_GET['date_of_departure']; } ?>"><br>
        <select required class="form-group-last" name="carrier">
            <option value="">Перевізник</option>
            @foreach($getCarrier as $trip)
                <option value="{{ $trip->carrier}}"{{ ( $trip->carrier == $carrier ) ? 'selected' : '' }}>{{ $trip->carrier }}</option>
            @endforeach
        </select>

        <input class="menu2" type="submit" value="Додати"/>
    </form>
    @include('app.nstatic.printTrip')
    @endauth
    @guest()
        <H1  align="center">Увійдіть в акаутн щоб додати рейси</H1>
        <br>
        <br>
        <br>
        <br>
        <br>
    @endguest
@endsection
