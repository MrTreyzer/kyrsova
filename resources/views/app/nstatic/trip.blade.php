@extends('app.static.layout')
@section("first")
    @include('app.nstatic.firstText')
@endsection
@section('content')

    <form id="bus" align="center" action="/trip">
        <nav>Рейси атобусів відсортованих</nav><br>
        <select class="form-group-last" name="sort_id">
            <option value="1" {{ ( 1 == $sort_id ) ? 'selected' : '' }}>Місце призначення</option>
            <option value="2" {{ ( 2 == $sort_id ) ? 'selected' : '' }}>Номер рейсу</option>
            <option value="3" {{ ( 3 == $sort_id ) ? 'selected' : '' }}>Дата прибуття</option>
            <option value="4" {{ ( 4 == $sort_id ) ? 'selected' : '' }}>Перевізник</option>
            <option value="5" {{ ( 5 == $sort_id ) ? 'selected' : '' }}>Продано білетів</option>
        </select>

        <input class="menu2" type="submit" value="Показати"/>
    </form>
    @include('app.nstatic.printTrip')
@endsection
