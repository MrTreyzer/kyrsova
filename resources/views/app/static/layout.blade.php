<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset("css.css") }}">
    <link rel="shortcut icon" href="{{ asset("favicon.png") }}" type="image/x-icon">
    <link rel="" href="" type="">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <title>Доставка Людей</title>
</head>
<body>
<div  id="first">
    <div class="telo">
        @include('app.static.menu')
        @yield('first')
    </div>
</div>

<div class="telo">
    @yield('page_title')
    <br/><br/>
    <div class="container">
        @yield('content')
    </div>
    <br/>
    <br/>

</div>
@include('app.static.footer')
</body>
</html>
