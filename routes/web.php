<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\raisController;
use \App\Http\Controllers\TripEditController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app.nstatic.home');
});

Route::get('trip',[raisController::class,'trip']);

Route::get('trip/{id}',[raisController::class,'oneTrip']);

Route::get('findTrip',[raisController::class,'findTrip']);

Route::get('addTrip',[TripEditController::class,'addTrip']);

Route::get('updateTrip',[TripEditController::class,'updateTrip']);

Route::get('delTrip',[TripEditController::class,'delTrip']);

Route::redirect('/','trip');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
